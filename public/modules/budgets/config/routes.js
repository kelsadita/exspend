'use strict';

//Setting up route
angular.module('exspend.budgets').config(['$stateProvider',
  function($stateProvider) {
        // Articles state routing
        $stateProvider.
        state('listBudgets', {
            url: '/budgets',
            templateUrl: 'modules/budgets/views/list.html'
        }).
        state('createBudget', {
            url: '/budgets/create',
            templateUrl: 'modules/budgets/views/create.html'
        }).
        state('viewBudget', {
            url: '/budgets/:articleId',
            templateUrl: 'modules/budgets/views/view.html'
        }).
        state('editBudget', {
            url: '/budgets/:articleId/edit',
            templateUrl: 'modules/budgets/views/edit.html'
        });
    }
]);