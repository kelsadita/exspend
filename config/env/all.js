'use strict';

var path = require('path'),
	rootPath = path.normalize(__dirname + '/../..');

// Getting the base url
var baseUrl = process.env.NODE_ENV  === 'development' ? 'http://localhost:3000' : 'http://test.com';

module.exports = {
	app: {
		title: 'exSpend',
		description: 'An application to track expence.',
		keywords: 'mongodb, express, angularjs, node.js, mongoose, passport'
	},
	db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI,
	root: rootPath,
	baseUrl: baseUrl,
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'DC',
	sessionCollection: 'sessions'
};