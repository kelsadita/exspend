'use strict';

module.exports = {
	//db: 'mongodb://kalpesh:elberteinestine@paulo.mongohq.com:10004/exspend',
	//db: 'mongodb://test:elberteinestine2@troup.mongohq.com:10005/testExSpend',
	db: 'mongodb://localhost/exspend',
	app: {
		title: 'exSpend-dev'
	},
	facebook: {
		clientID: 'APP_ID',
		clientSecret: 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/facebook/callback'
	},
	twitter: {
		clientID: 'CONSUMER_KEY',
		clientSecret: 'CONSUMER_SECRET',
		callbackURL: 'http://localhost:3000/auth/twitter/callback'
	},
	google: {
		clientID: 'APP_ID',
		clientSecret: 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/google/callback'
	},
	linkedin: {
		clientID: 'APP_ID',
		clientSecret: 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/linkedin/callback'
	}
};