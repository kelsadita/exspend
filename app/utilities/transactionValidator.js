'use strict';

var validator = require('express-validator');

/**
*
* Function to validate add transaction payload
*
**/

exports.validateAddTransaction = function (request) {
    request.assert('amount', 'should be number').isInt();
    request.assert('satisfaction_level', 'should not be null').notEmpty();
    request.assert('budget', 'should not be null').notEmpty();
    request.assert('created', 'should not be null').notEmpty();

    return request.validationErrors();
};

/**
*
* Function to validate update transaction payload
*
**/

exports.validateUpdateTransaction = function (request) {

    // Either one should present for update
    if (!request.body.amount && !request.body.satisfaction_level) {
        return false;
    }

    // If only amount present - validate it!
    else if(request.body.amount && !request.body.satisfaction_level){
        request.assert('amount', 'should be number').isInt();
    }

    // If only satisfaction_level present - validate it!
    else if(!request.body.amount && request.body.satisfaction_level){
        request.assert('satisfaction_level', 'should not be null').notEmpty();
    }

    // If both are present - validate both!
    else{
        request.assert('amount', 'should be number').isInt();
        request.assert('satisfaction_level', 'should not be null').notEmpty();
    }

    return request.validationErrors();
};


