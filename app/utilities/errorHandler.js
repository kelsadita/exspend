'use strict';

function errorResponseBuilder(){
    return {
        addMessage: function (message) {
            this.message = message;
            return this;
        },
        addErrorObj: function (errObj) {
            this.errObj = errObj;
            return this;
        },
        addStatusCode: function (statusCode) {
            this.statusCode = statusCode;
            return this;
        },
        build: function () {
            return{
                message: this.message,
                errorObject: this.errObj,
                statusCode: this.statusCode
            };
        }
    };
}

exports.createErrorResponse = function (errorCode, errObj, message) {
    var errorDetails = new errorResponseBuilder()
                                .addMessage(message)
                                .addErrorObj(errObj)
                                .addStatusCode(errorCode)
                                .build();

    return errorDetails;
};