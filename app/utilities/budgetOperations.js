'use strict';

var mongoose = require('mongoose'),
    Budget = mongoose.model('Budget'),
    ObjectId = mongoose.Types.ObjectId,
    Q = require('q');

/**
*
* Get the budget entry for specified budget ID
*
**/

exports.getBudget = function(budget_id){
    var amount = 0,
        query = {_id: budget_id};

    var deffered = Q.defer();
    Budget
    .find(query, function (err, data) {
        if (err) {
            deffered.reject(err);
        }else{
            deffered.resolve(data);
        }
    });
    return deffered.promise;
};

/**
*
* Debit the budget on addition of a transaction
*
**/

exports.debitBudget = function(budget_info, txn_amount) {
    var query = {_id: budget_info._id},
        deffered = Q.defer(),
        error;

    var update_data = {
        amount_spent: budget_info.amount_spent + txn_amount,
        amount_remaining: budget_info.amount - (budget_info.amount_spent + txn_amount),
        last_update: new Date()
    };

    // Reject promise if amount to be added is greater then budget remaining
    if (update_data.amount_remaining < 0) {
        error = {
            message: 'Amount to be added is greater then remaining budget',
        };
        deffered.reject(error);
    }

    Budget
    .update(query, update_data, function (err, data) {
        if (err) {
            deffered.reject(err);
        }else{
            deffered.resolve(data);
        }
    });

    return deffered.promise;
};

/**
*
* Delete the specified budget entry
*
**/

exports.deleteBudget = function (budget_id) {
    var query = {_id: new ObjectId(budget_id)},
        deffered = Q.defer();

    Budget
    .remove(query, function (err) {
        if (err) {
            deffered.reject(err);
        }else{
            deffered.resolve();
        }
    });
    return deffered.promise;
};