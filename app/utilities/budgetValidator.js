'use strict';

var validator = require('express-validator');

exports.validateCreateBudget = function (request) {
    request.assert('amount', 'should be number').isInt();
    request.assert('from_date', 'should not be null').notEmpty();
    request.assert('to_date', 'should not be null').notEmpty();
    request.assert('created', 'should not be null').notEmpty();

    return request.validationErrors();
};