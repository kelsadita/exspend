'use strict';

var mongoose = require('mongoose'),
    Transaction = mongoose.model('Transaction'),
    Q = require('q');

/**
*
* Function to add transaction in the database and return proper response
*
**/

exports.createTransactionEntry = function (req, res) {
    var transaction = new Transaction(req.body),
        deffered = Q.defer();
    transaction.save(function (err, transactionEntry) {
        if (err) {
            deffered.reject(err);
        } else {
            deffered.resolve(transactionEntry);
        }
    });
    return deffered.promise;
};

/**
*
* Function to get the information about the single transaction
*
**/


exports.getTransaction = function (query) {
    var deffered = Q.defer();
    Transaction
    .find(query)
    .exec(function (err, data) {
        if (err) {
            deffered.reject(err);
        }else{
            deffered.resolve(data);
        }
    });
    return deffered.promise;
};


/**
*
* Function to get list of transactio for given budget
*
**/

exports.getTransactions = function (query) {
    var deffered = Q.defer();
    Transaction
    .find(query)
    .sort('-created')
    .exec(function (err, data) {
        if (err) {
            deffered.reject(err);
        }else{
            deffered.resolve(data);
        }
    });
    return deffered.promise;
};

/**
*
* Function to update given transaction entry
*
**/

exports.updateTransaction = function (txn_update_data) {
    var deffered = Q.defer(),
        query = {_id: txn_update_data._id},
        updateData = {
            amount: txn_update_data.amount,
            satisfaction_level: txn_update_data.satisfaction_level
        };

    Transaction
    .update(query, updateData, function (err, data) {
        if (err) {
            deffered.reject(err);
        }else{
            deffered.resolve(data);
        }
    });

    return deffered.promise;
};

/**
*
* Function to delete given transaction 
*
**/

exports.deleteTransaction = function (txn_id) {
    var deffered = Q.defer(),
        query = {_id: txn_id};

    Transaction
    .remove(query, function (err) {
        if (err) {
            deffered.reject(err);
        }else{
            deffered.resolve();
        }
    });

    return deffered.promise;
};

/**
*
* Function that deletes transactions related to specified budget ID
*
**/

exports.deleteTransactions = function (budget_id) {
    var deffered = Q.defer(),
        query = {budget: budget_id};

    Transaction
    .remove(query, function (err, data) {
        if (err) {
            deffered.reject(err);
        }else{
            deffered.resolve(data);
        }
    });

    return deffered.promise;
};