'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var Transaction = new Schema({

    amount: {
        type: Number,
        required: 'Please enter the amount of transaction.',
        min: 1
    },
    created: {
        type: Date,
        default: Date.now
    },
    satisfaction_level: {
        type: String,
        required: 'Please specify the satisfoactory level.'
    },
    budget: {
        type: Schema.ObjectId,
        ref: 'Budget'
    }
});

mongoose.model('Transaction', Transaction);