'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var Budget = new Schema({

    created: {
        type: Date,
        default: Date.now
    },
    amount: {
        type: Number,
        required: 'Please enter your budget.',
        min: 1
    },
    amount_spent: {
        type: Number,
        default: 0
    },
    amount_remaining: {
        type: Number,
        default: 0
    },
    from_date: {
        type: Date,
        required: 'Please enter the starting date for budget.'
    },
    to_date: {
        type: Date,
        required: 'Please enter the ending date for budget.'
    },
    last_update: {
        type: Date,
    }
});

mongoose.model('Budget', Budget);