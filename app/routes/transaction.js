'use strict';

module.exports = function (app) {
    var transactions = require('../../app/controllers/transactions');

    // Transaction routes
    app.get('/exspend/api/budgets/:budgetId/transactions', transactions.getTransactions);
    app.post('/exspend/api/budgets/:budgetId/transactions', transactions.addTransaction);
    app.put('/exspend/api/budgets/:budgetId/transactions/:transactionId', transactions.updateTransaction);
    app.delete('/exspend/api/budgets/:budgetId/transactions/:transactionId', transactions.deleteTransaction);
};