'use strict';

module.exports = function (app) {
    var budgets = require('../../app/controllers/budgets');

    // Budget Routes
    app.get('/exspend/api/budgets', budgets.getBudgets);
    app.post('/exspend/api/budgets', budgets.createBudget);
    app.delete('/exspend/api/budgets/:budgetId', budgets.deleteBudget);
};
