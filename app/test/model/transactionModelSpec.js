'use strict';

var should = require('should'),
    mongoose = require('mongoose'),
    Transaction = mongoose.model('Transaction'),
    Budget = mongoose.model('Budget');

// variavbles for transactions
var transaction1, transaction2;

// variable for budgets
var budget1;

describe('<Transaction Model Unit Test>', function () {
    describe('Model Transaction:', function () {
        before(function (done) {

            budget1 = new Budget({
                created: new Date(),
                amount: 3000,
                from_date: new Date(),
                to_date: new Date()
            });

            done();
        });

        it('Should save the transaction for the specific budget', function (done) {
            budget1.save(function (err, budget) {
                should.equal(err, null);
                var budget_id = budget._id;
                transaction1 = new Transaction({
                    amount: 1000,
                    created: new Date(),
                    satisfaction_level: 'worth',
                    budget: budget_id
                });

                transaction1.save(function (err) {
                    should.equal(err, null);
                    done();
                });
            });
        });

        it('Should save the transaction for the specific budget and retrieve budget document when required', function (done) {
            var txnID;
            budget1.save(function (err, budget) {
                should.equal(err, null);
                var budget_id = budget._id;
                transaction1 = new Transaction({
                    amount: 1000,
                    created: new Date(),
                    satisfaction_level: 'worth',
                    budget: budget_id
                });

                transaction1.save(function (err, data) {
                    should.equal(err, null);
                    txnID = data._id;

                    Transaction
                        .findOne({_id: txnID})
                        .populate('budget')
                        .exec(function (err, data) {
                            should.equal(err, null);
                        });

                    done();
                });

            });
        });

        // Cleaning
        after(function(done) {
            Budget.remove().exec();
            Transaction.remove().exec();
            done();
        });
    });
});