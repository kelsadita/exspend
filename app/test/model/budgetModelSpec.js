'use strict';

var should = require('should'),
    mongoose = require('mongoose'),
    Budget = mongoose.model('Budget');

// Globals
var budget1, budget2;

describe('<Budget Model Unit Test>', function() {
    describe('Model Budget:', function() {
        before(function(done) {
            budget1 = new Budget({
                created: new Date(),
                amount: 3000,
                from_date: new Date(),
                to_date: new Date()
            });
            budget2 = new Budget({
                created: new Date(),
                amount: 2000,
                from_date: new Date(),
                to_date: new Date()
            });

            done();
        });

        describe('Method Save', function() {
            it('should begin with no budget', function(done) {
                this.timeout(5000);
                Budget.find({}, function(err, budgets) {
                    budgets.should.have.length(0);
                    done();
                });
            });

            it('should be able to save whithout problems', function(done) {
                budget1.save(done);
            });

            it('should be able to show an error when try to save without amount', function(done) {
                budget1.amount = 0;
                return budget1.save(function(err) {
                    should.exist(err);
                    done();
                });
            });
        });

        // Cleaning
        after(function(done) {
            Budget.remove().exec();
            done();
        });
    });
});