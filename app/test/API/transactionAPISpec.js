'use strict';

var mongoose = require('mongoose'),
    config  = require('../../../config/config'),
    request = require('supertest'),
    should = require('should'),
    Transaction = mongoose.model('Transaction'),
    Budget = mongoose.model('Budget');

// Budget related variables
var budget1, budget_id;

// Transaction related variables
var transaction1, transaction2, txn_id1;

describe('Transaction API Testing', function () {
    
    before(function (done) {
        this.timeout(5000);

        // Initiating test budget
        budget1 = new Budget({
            created: new Date(),
            amount: 3000,
            from_date: new Date(),
            to_date: new Date()
        });

        budget1.save(function(err, data){
            budget_id =  data._id.toString();

            // Adding test transactions
            var txnData1 = {
                amount: 1000,
                created: new Date(),
                satisfaction_level: 'worth',
                budget: budget_id
            };
            transaction1 = new Transaction(txnData1);
            transaction1.save(function (err, data) {
                txn_id1 = data._id.toString();
                var txnData2 = {
                    amount: 2000,
                    created: new Date(),
                    satisfaction_level: 'not worth',
                    budget: budget_id
                };
                transaction2 = new Transaction(txnData2);
                transaction2.save(function (err, data) {
                    done();
                });

            });
        });
        
    });


    describe('GET /exspend/api/budgets/{budget-id}/transactions', function () {
        it('should return list of transactions corresponding to a specific budget', function (done) {
            this.timeout(5000);
            request(config.baseUrl)
                .get('/exspend/api/budgets/' + budget_id + '/transactions')
                .expect(200)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .end(function (err, res) {
                    if (err) {
                        should.not.exist(err);
                    }
                    res.body[0].should.have.property('satisfaction_level');
                    done();
                });
        });
    });

    describe('POST /exspend/api/budgets/{budget-id}/transactions', function () {
        it('Should create new transaction entry corresponding to specific budget', function (done) {
            this.timeout(5000);

            var testData = {
                created: new Date(),
                amount: 1000,
                satisfaction_level: 'obligatory',
                budget: budget_id
            };

            request(config.baseUrl)
                .post('/exspend/api/budgets/' + budget_id + '/transactions')
                .send(testData)
                .expect(200)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .end(function (err, res) {
                    if (err) {
                        should.not.exist(err);
                    }
                    res.body.should.have.property('satisfaction_level');
                    done();
                });
        });
    });

    describe('PUT /exspend/api/budgets/{budget-id}/transacitons/{txn-id}', function () {
        it('Should update transaciton as well as debit the budget', function (done) {
            this.timeout(5000);

            var txnUpdateData = {
                amount: 500,
                satisfaction_level: 'not worth'
            };

            request(config.baseUrl)
            .put('/exspend/api/budgets/' + budget_id + '/transactions/' + txn_id1)
            .send(txnUpdateData)
            .expect(200)
            .expect('Content-Type', 'application/json; charset=utf-8')
            .end(function (err, res) {
                if (err) {
                    console.log(err);
                    should.not.exist(err);
                    done();
                }else{
                    Transaction
                    .find({_id:txn_id1}, function (err, data) {
                        if (err) {
                            console.log(err);
                            should.not.exist(err);
                        }else{
                            data[0].should.have.property('amount').equal(500);
                        }
                        done();
                    });
                }
            });
        });
    });

    describe('DELETE /exspend/api/budgets/{budget-id}/transactions/{txn-id}', function (done) {
        it('Should delete transaciton as well as debit the budget', function (done) {
            this.timeout(5000);
            
            request(config.baseUrl)
                .del('/exspend/api/budgets/' + budget_id + '/transactions/' + txn_id1)
                .expect(200)
                .expect('Content-Type', 'application/json; charset=utf-8')
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        should.not.exist(err);
                    }
                    should.equal(res.body.amount, 500);
                    should.equal(res.body.satisfaction_level, 'not worth');
                    done();
                });
        });
    });

    // Cleaning
    after(function (done) {
        Budget.remove().exec();
        Transaction.remove().exec();
        done();
    });
});