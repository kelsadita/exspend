'use strict';

var mongoose = require('mongoose'),
    config = require('../../../config/config'),
    request = require('supertest'),
    should = require('should'),
    Budget = mongoose.model('Budget'),
    Transaction = mongoose.model('Transaction');

// Budgets related variables
var budget1, budget_id;

// Transaction related variables
var transaction1, transaction2, txn_id1;

describe('Budget API Testing', function(){

    before(function (done) {
        // Initiating test budget
        budget1 = new Budget({
            created: new Date(),
            amount: 3000,
            from_date: new Date(),
            to_date: new Date()
        });

        budget1.save(function(err, data){
            budget_id =  data._id.toString();

            // Adding test transactions
            var txnData1 = {
                amount: 1000,
                created: new Date(),
                satisfaction_level: 'worth',
                budget: budget_id
            };
            transaction1 = new Transaction(txnData1);
            transaction1.save(function (err, data) {
                txn_id1 = data._id.toString();
                var txnData2 = {
                    amount: 2000,
                    created: new Date(),
                    satisfaction_level: 'not worth',
                    budget: budget_id
                };
                transaction2 = new Transaction(txnData2);
                transaction2.save(function (err, data) {
                    done();
                });

            });
        });
    });

    describe('GET /exspend/api/budgets : ', function () {
        it('Should return success response', function (done) {
            this.timeout(5000);
            request(config.baseUrl)
                .get('/exspend/api/budgets')
                .expect('content-Type', 'application/json; charset=utf-8')
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        should.not.exist(err);
                    }
                    res.body.length.should.equal(1);
                    done();
                });
        });
    });

    describe('POST /exspend/api/budgets - successful creation', function () {
        
        it('should add a budget entry', function (done) {
            this.timeout(5000);
            var testData = {
                created: new Date(),
                amount: 3000,
                from_date: new Date(),
                to_date: new Date()
            };
            request(config.baseUrl)
                .post('/exspend/api/budgets')
                .send(testData)
                .expect('content-Type', 'application/json; charset=utf-8')
                .expect(201)
                .end(function (err, res) {
                    if (err) {
                        should.not.exist(err);
                    }
                    done();
                });
        });
    });

    describe('DELETE /exspend/api/budgets/{budget-id} - successful deletion with removal of corresponding transactions', function () {
        it('should delere budget', function (done) {
            this.timeout(5000);

            request(config.baseUrl)
                .del('/exspend/api/budgets/' + budget_id)
                .expect(200)
                .expect('content-Type', 'application/json; charset=utf-8')
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        should.not.exists(err);
                    }
                    
                    done();
                });
        });
    });

    // Cleaning all the documents at the end of the test
    after(function(done) {
        Budget.remove().exec();
        done();
    });
});
