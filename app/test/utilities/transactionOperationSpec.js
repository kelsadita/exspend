'use strict';

var mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    Transaction = mongoose.model('Transaction'),
    Budget = mongoose.model('Budget'),
    transactionOperations = require('../../utilities/transactionOperations'),
    should = require('should');

// Budget related variables
var budget1, budget_id;

// Transaction related variables
var transaction1, txn_id;

describe('<Transaction Operations Unit Testing>', function () {

    before(function (done) {
        
        budget1 = new Budget({
            created: new Date(),
            amount: 3000,
            from_date: new Date(),
            to_date: new Date()
        });

        budget1.save(function (err, budget) {
            should.equal(err, null);
            budget_id = budget._id;
            transaction1 = new Transaction({
                amount: 1000,
                created: new Date(),
                satisfaction_level: 'worth',
                budget: budget_id
            });

            transaction1.save(function (err, data) {
                should.equal(err, null);
                txn_id = data._id;
                done();
            });
        });

    });

    it('Create Transaction: for given request', function (done) {
        var request = {};
        request.body = {
            created: new Date(),
            amount: 1000,
            satisfaction_level: 'obligatory',
            budget: new ObjectId('testtesttett')
        };

        transactionOperations.createTransactionEntry(request, null)
        .then(function (data) {
            should.exist(data);
            done();
        },
        function (err) {
            console.log(err);
            should.not.exist(err);
            done();
        });
    });

    it('Get Single Transaction: should retrieve the information about the single transaction', function (done) {
        var query = {_id: txn_id};
        transactionOperations.getTransaction(query)
        .then(function (data) {
            should.equal(data[0].amount, 1000);
            done();
        }, function (err) {
            should.not.exist(err);
            done();
        });
    });

    it('Get Transaction List: should get the list of transactions for given budget', function  (done) {
        var query = {budget: new ObjectId(budget_id.toString())};
        transactionOperations.getTransactions(query)
        .then(function(data){
            should.exist(data);
            done();
        },
        function (err) {
            console.log(err);
            done();
        });

    });

    it('Update Transaction: should update transaction along with updating given budget values', function (done) {
        var txn_data = {
            _id: txn_id,
            amount: 500,
            satisfaction_level: 'not worth'
        };

        transactionOperations.updateTransaction(txn_data )
        
        // verifying successful update execution
        .then(function (data) {
            should.equal(data, 1);
            return transactionOperations.getTransactions({_id: txn_id});
        },
        function (err) {
            should.not.exist(err);
            done();
        })

        // verifying successful update
        .then(function (data) {
            data[0].should.have.property('amount').equal(500);
            data[0].should.have.property('satisfaction_level').equal('not worth');
            done();
        },
        function (err) {
            should.not.exist(err);
            done();
        });
    });

    it('Delete Transaction: should delete transaction by crediting transaction amount from budget', function (done) {
        transactionOperations.deleteTransaction(txn_id)
        .then(function () {
            done();
        },
        function (err) {
            should.not.exist(err);
            done();
        });

    });

    it('Delete transactions: delete list of transactions asscociated with the specified budget', function (done) {
        transactionOperations.deleteTransactions(budget_id)
        .then(function (data) {
            should.exist(data);
            return transactionOperations.getTransaction({_id: txn_id});
        },
        function (err) {
            console.log(err);
            should.not.exist(err);
            done();
        })
        .then(function (data) {
            data.should.have.length(0);
            done();
        },
        function (err) {
            console.log(err);
            should.not.exist(err);
            done();
        });
    });

    after(function (done) {
        Transaction.remove().exec();
        done();
    });
});