'use strict';

var budgetValidator = require('../../utilities/budgetValidator'),
    should = require('should'),
    request = require('supertest'),
    config = require('../../../config/config');

describe('<Budget Utiliy Unit testing>', function () {
    describe('POST /exspend/api/budgets - Validate create budget', function () {
        it('should throw an error for invalid budget entry', function (done) {
            this.timeout(5000);
            var testData = {
                created: new Date(),
                from_date: new Date(),
            };
            request(config.baseUrl)
                .post('/exspend/api/budgets')
                .send(testData)
                .expect('content-Type', 'application/json; charset=utf-8')
                .expect(400)
                .end(function (err, res) {
                    res.body.should.have.property('errorObject');
                    res.body.errorObject.should.have.length(2);
                    done();
                });
        });
    });
});