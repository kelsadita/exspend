'use strict';

var transactionValidator = require('../../utilities/transactionValidator'),
    should = require('should'),
    request = require('supertest'),
    config = require('../../../config/config'),
    mongoose = require('mongoose'),
    Budget = mongoose.model('Budget'),
    Transaction = mongoose.model('Transaction');

// budget related variables
var budget1, budget2, budget_id;

// Transaction related variables
var transaction1, txn_id;

describe('<Transaction Utiliy Unit testing>', function () {
    describe('POST /exspend/api/budgets/{budget-id} - Validate add trasaction', function () {

        before(function () {
            // Initiating test budget
            budget1 = new Budget({
                created: new Date(),
                amount: 3000,
                from_date: new Date(),
                to_date: new Date()
            });

            budget1.save(function(err, data){
                budget_id =  data._id;
            });
        });

        it('should throw an error for invalid trasaction entry', function (done) {
            this.timeout(5000);
            var testData = {
                created: new Date(),
            };
            request(config.baseUrl)
                .post('/exspend/api/budgets/' + budget_id + '/transactions')
                .send(testData)
                .expect('content-Type', 'application/json; charset=utf-8')
                .expect(400)
                .end(function (err, res) {
                    res.body.should.have.property('errorObject');
                    res.body.errorObject.should.have.length(3);
                    done();
                });
        });
    });

    describe('PUT /exspend/api/budgets/{budget-id}/transactions/{txn-id} - Validate update trasaction', function () {
       
        before(function (done) {
            // Initiating test budget
            budget2 = new Budget({
                created: new Date(),
                amount: 2000,
                from_date: new Date(),
                to_date: new Date()
            });

            budget2.save(function(err, data){
                budget_id =  data._id;
                transaction1 = new Transaction({
                    amount: 1000,
                    created: new Date(),
                    satisfaction_level: 'worth',
                    budget: budget_id
                });

                transaction1.save(function (err, data) {
                    should.equal(err, null);
                    txn_id = data._id;
                    done();
                });
            });
        });

        it('should throw an error for invalid trasaction update entry', function (done) {
            this.timeout(5000);
            var testData = {
                // no payload
            };

            request(config.baseUrl)
                .put('/exspend/api/budgets/' + budget_id + '/transactions/' + txn_id)
                .send(testData)
                .expect('content-Type', 'application/json; charset=utf-8')
                .expect(400)
                .end(function (err, res) {
                    res.body.should.have.property('errorObject');
                    res.body.should.have.property('message').equal('Update data is not provided.');
                    done();
                });
        });

        it('should throw an error for invalid trasaction update entry - amount is string', function (done) {
            this.timeout(5000);
            var testData = {
                amount: 'test'
            };

            request(config.baseUrl)
                .put('/exspend/api/budgets/' + budget_id + '/transactions/' + txn_id)
                .send(testData)
                .expect('content-Type', 'application/json; charset=utf-8')
                .expect(400)
                .end(function (err, res) {
                    res.body.should.have.property('errorObject');
                    res.body.should.have.property('message').equal('Transaction data to update is invalid.');
                    done();
                });
        });
    });

    // Cleaning
    after(function (done) {
        Budget.remove().exec();
        Transaction.remove().exec();
        done();
    });

});