'use strict';

var createErrorResponse = require('../../utilities/errorHandler').createErrorResponse,
    should = require('should');

describe('<Error Handler Unit Testing>', function  () {
    
    it('Should generate proper error response in JSON', function () {
        var errorResponse = createErrorResponse(500, {}, 'This is a test error.');
        errorResponse.should.have.property('statusCode');
        errorResponse.should.have.property('errorObject');
        errorResponse.should.have.property('message');
    });

});