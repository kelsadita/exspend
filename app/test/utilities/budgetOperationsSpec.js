'use strict';

var budgetOps = require('../../utilities/budgetOperations'),
    transactionOps = require('../../utilities/transactionOperations'),
    mongoose = require('mongoose'),
    Budget = mongoose.model('Budget'),
    should = require('should');

var budget1, budget_id;

describe('<Budget operations unit testing>', function () {

    before(function (done) {
        // Initiating test budget
        budget1 = new Budget({
            created: new Date(),
            amount: 3000,
            from_date: new Date(),
            to_date: new Date()
        });

        budget1.save(function(err, data){
            budget_id =  data._id;
            done();
        });
    });

    it('Get Budget: Should get the information about specified budget.', function (done) {
        budgetOps.getBudget(budget_id).then(function (data) {
            should.equal(data[0].amount_spent, 0);
            should.equal(data[0].amount_remaining, 0);
            done();
        },
        function (err) {
            should.not.exist(err);
            done();
        });
    });

    it('Debit budget: Should debit certain amount from budget of given id', function (done) {
        this.timeout(5000);
        
        budgetOps
        .getBudget(budget_id)
        .then(function (data) {
            return budgetOps.debitBudget(data[0], 1000);
        }, function (err) {
            console.log(err);
            should.not.exist(err);
            done();
        })
        .then(function(data){
            return budgetOps.getBudget(budget_id);
        },
        function (err) {
            console.log(err);
            should.not.exist(err);
            done();
        })
        .then(function(data){
            data[0].should.have.property('amount_spent').equal(1000);
            data[0].should.have.property('amount_remaining').equal(2000);
            done();
        },
        function (err) {
            console.log(err);
            should.not.exist(err);
            done();
        });
        
    });

    it('Debit Budget Error: should raise an error before debiting amount greater then ramaining amount.', function (done) {
        this.timeout(5000);
        
        budgetOps
        .getBudget(budget_id)
        .then(function (data) {
            return budgetOps.debitBudget(data[0], 10000);
        },
        function (err) {
            console.log(err);
            should.not.exist(err);
            done();
        })
        .then(function(data){
            console.log(data);
            should.not.exist(data);
            done();
        },
        function (err) {
            should.exist(err);
            done();
        });
    });

    it('Delete budget: should delete budget entries', function (done) {
        this.timeout(5000);

        budgetOps.deleteBudget(budget_id.toString())
        .then(function (data) {
            return budgetOps.getBudget(budget_id.toString());
        })
        .then(function (data) {
            data.should.have.length(0);
            done();
        },
        function (err) {
            console.log(err);
            should.not.exist(err);
            done();
        });
    });

    // Cleaning all the documents at the end of the test
    after(function(done) {
        Budget.remove().exec();
        done();
    });
});