'use strict';

var mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    Transaction = mongoose.model('Transaction'),
    createErrorResponse = require('../utilities/errorHandler').createErrorResponse,
    transactionValidator = require('../utilities/transactionValidator'),
    budgetOperations = require('../utilities/budgetOperations'),
    transactionOperations = require('../utilities/transactionOperations');
	
// Getting all the available transactions a specific budget
exports.getTransactions = function (req, res) {
    var budgetId = req.params.budgetId;
    var query = {budget: new ObjectId(budgetId)};
    transactionOperations.getTransactions(query)
    .then(function (transactionsData) {
        res.json(transactionsData);
    },
    function (err) {
        res.statusCode = 500;
        res.json(createErrorResponse(res.statusCode, err, 'Error in getting list of available transactions.'));
    });
};

// Creating new transaction for given budget
exports.addTransaction = function (req, res) {
    var requestErrors = transactionValidator.validateAddTransaction(req);
    if (requestErrors) {
        res.statusCode = 400;
        res.json(createErrorResponse(res.statusCode, requestErrors, 'Error in request parameters for creating budgte entry.'));
    } else {

        var budgetId = req.params.budgetId;
        
        // Step 1: Get budget data and debit budget
        budgetOperations.getBudget(budgetId)
        .then(function (budgetInfo) {
            return budgetOperations.debitBudget(budgetInfo[0], 1000);
        },
        function (err) {
            res.statusCode = 500;
            res.json(createErrorResponse(res.statusCode, err, 'Error in adding transaction for given budget.'));
        })

        // Step 2: When above promise of debiting transaction is resolve add transaction
        .then(function (data) {
            return transactionOperations.createTransactionEntry(req, res);
        },
        function (err) {
            res.statusCode = 500;
            res.json(createErrorResponse(res.statusCode, err, 'Error in adding transaction for given budget.'));
        })

        // Step 3: Send response for transaction processing
        .then(function (transactionEntry) {
            res.statusCode = 200;
            res.json(transactionEntry);
        },
        function (err) {
            res.statusCode = 500;
            res.json(createErrorResponse(res.statusCode, err, 'Error in adding transaction for given budget.'));
        });
    }
};

// Updating a transaction
exports.updateTransaction = function (req, res) {
    var txn_id = req.params.transactionId,
        budget_id = req.params.budgetId,
        txn_update_amount = req.body.amount,
        txn_satisfaction_level = req.body.satisfaction_level,
        absolute_txn_amount,
        txn_data,
        txn_update_data = {};

    // Step 1: validate payload has required data
    var requestErrors = transactionValidator.validateUpdateTransaction(req);
    if (requestErrors === false) {
        res.statusCode = 400;
        res.json(createErrorResponse(res.statusCode, requestErrors, 'Update data is not provided.'));
    }else if(requestErrors){
        res.statusCode = 400;
        res.json(createErrorResponse(res.statusCode, requestErrors, 'Transaction data to update is invalid.'));
    }else{

        // Step 2: Get the transaction and budget information
        var txn_query = {_id: new ObjectId(txn_id)};
        transactionOperations.getTransaction(txn_query)

        // Step 3: Calculate the absolute tranasaction amount
        .then(function (txnData) {
            txn_data = txnData[0];
            absolute_txn_amount = txn_data.amount - txn_update_amount;
            return budgetOperations.getBudget(budget_id);
        })

        // Step 4: Debit budget amount with updated transaction amount
        .then(function (budgetData) {
            return budgetOperations.debitBudget(budgetData[0], -1 * absolute_txn_amount);
        })

        // Step 5: Update the transaction amount
        .then(function () {
            txn_update_data._id = new ObjectId(txn_id);
            if (txn_update_amount !== undefined) {
                txn_update_data.amount = txn_update_amount;
            }
            if(txn_satisfaction_level !== undefined){
                txn_update_data.satisfaction_level = txn_satisfaction_level;
            }
            return transactionOperations.updateTransaction(txn_update_data);
        })

        // Step 6: Get updated transaction
        .then(function () {
            return transactionOperations.getTransaction(txn_query);
        })

        // Step 7: Send updated transaction in response
        .then(function (txnData) {
            res.statusCode = 200;
            res.json(txnData);
        })

        // Step 8: Error handling
        .then(undefined, function (err) {
            res.statusCode = 500;
            console.log(err);
            res.json(createErrorResponse(res.statusCode, requestErrors, 'Failed to update transaction.'));
        });
    }
};

// Deleting a transaction
// TODO Fix this
exports.deleteTransaction = function(req, res){
    var txn_id = req.params.transactionId,
        budget_id = req.params.budgetId,
        txn_data,
        txn_query = {_id: new ObjectId(txn_id)};

    // Step 1: get the information about the transaction
    transactionOperations.getTransaction(txn_query)
    
    // Step 2: Delete transaction    
    .then(function(txnData) {
        txn_data = txnData[0];
        return transactionOperations.deleteTransaction(txn_id, budget_id);
    })

    // Step 3: after deleting transaction, get the budget
    .then(function () {
        return budgetOperations.getBudget(budget_id);
    })

    // Step 4: Debit the budget entry with given transaction amount
    .then(function (budgetData) {
        return budgetOperations.debitBudget(budgetData[0], txn_data.amount);
    })

    // Step 5: Handle the error at the end if any
    .then(function () {
        res.statusCode = 200;
        res.json(txn_data);
    },
    function (err) {
        res.statusCode = 500;
        res.json(createErrorResponse(res.statusCode, err, 'Error in deleting transaction for given budget.'));
    });
};