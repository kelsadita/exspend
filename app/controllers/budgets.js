'use strict';

var mongoose = require('mongoose'),
    Budget = mongoose.model('Budget'),
    createErrorResponse = require('../utilities/errorHandler.js').createErrorResponse,
    budgetValidator = require('../utilities/budgetValidator'),
    budgetOperations = require('../utilities/budgetOperations'),
    transactionOperations = require('../utilities/transactionOperations'),
    _ = require('lodash');

// Getting all available budgets
exports.getBudgets = function (req, res) {
    Budget
    .find()
    .sort('-created')
    .exec(function (err, budgets) {
        if (err) {
            res.statusCode = 500;
            res.json(createErrorResponse(res.statusCode, err, 'Error in getting list of available budgets.'));
        }else{
            res.json(budgets);
        }
    });
};

// Creating new budget entry
exports.createBudget = function (req, res) {
    var requestErrors = budgetValidator.validateCreateBudget(req);
    if (requestErrors) {
        res.statusCode = 400;
        res.json(createErrorResponse(res.statusCode, requestErrors, 'Error in request parameters for creating budget entry.'));
    }else{
        var requestData = _.assign(req.body, {last_update: new Date()});
        var budget = new Budget(requestData);
        budget.save(function (err, budgetEntry) {
            if (err) {
                res.statusCode = 500;
                res.json(createErrorResponse(res.statusCode, err, 'Error in creating budgte entry.'));
            } else {
                res.statusCode = 201;
                res.json(budgetEntry);
            }
        });
    }
};

// Deleting budget entry
exports.deleteBudget = function (req, res) {
    var budget_id = req.params.budgetId,
        budget_data;

    // Step 1: Getting budget data and deleting budget
    budgetOperations.getBudget(budget_id)
    .then(function (budgetData) {
        budget_data = budgetData[0];
        return budgetOperations.deleteBudget(budget_id);
    })
    
    // Step 2: Deleting all related transactions
    .then(function (data) {
        return transactionOperations.deleteTransactions(budget_id);
    })

    // Step 3: Handling errors if any or else sending success response
    .then(function () {
        res.statusCode = 200;
        res.json(budget_data);
    },
    function (err) {
        res.statusCode = 500;
        res.json(createErrorResponse(res.statusCode, err, 'Error in deleting requested budget.'));
    });
};
